/*http://rainbow.arch.scriptmania.com/scripts/tinkerbell_cursor_trail.html*/
var ofsm = [-2,1];
var cursorheight;
var cursorwidth;

function cursorImage(path, width, height){
	cursorheight = height;
	cursorwidth = width;	
	if (document.getElementById || document.all){
		if (path){
			document.write('<div id="trailimageid" style="position:absolute;visibility:hidden;display:none;left:0px;top:0px;width:1px;height:1px;z-index:200;"><img src="'+path+'" border="0" width="'+width+'px" height="'+height+'px"></div>');
			} else {
			document.write('<div id="trailimageid"></div>');				
			}
		}
	//don't follow until fully loaded:
	document.onmousemove = followmouse;
	}

function gettrailobj(){
	if (document.getElementById){
		return document.getElementById("trailimageid").style;
		} else if (document.all) {
		return document.all.trailimageid.style;
		}
	}
	
function truebody(){
	return (!window.opera && document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body;
	}
	
function followmouse(e){
	var xcoord = ofsm[0];
	var ycoord = ofsm[1];
	document.getElementById("trailimageid").style.visibility = "visible";
	document.getElementById("trailimageid").style.display = "block";
	if (typeof e != "undefined"){
		xcoord += e.pageX;
		ycoord += e.pageY;
		} else if (typeof window.event !="undefined"){
		xcoord += truebody().scrollLeft+event.clientX;
		ycoord += truebody().scrollTop+event.clientY;
		}
	var docwidth = document.all ? truebody().scrollLeft + truebody().clientWidth : window.pageXOffset + window.innerWidth - 15;
	var docheight = document.all ? truebody().scrollTop + truebody().clientHeight : window.pageYOffset + window.innerHeight;
	if (ycoord + cursorheight >= docheight || xcoord + cursorwidth + 3 >= docwidth){ //restrict for scrollbars
		//gettrailobj().display = "none";
		} else {
		gettrailobj().display = "";
		gettrailobj().left = xcoord+"px";
		gettrailobj().top = ycoord+"px";
		}
	}